
$$("#generate_result").hide();
$$("#generate_progressbar").hide();

var autocompleteDropdownSimple = myApp.autocomplete({
    input: '#autocomplete-dropdown',
    openIn: 'dropdown',
    limit: 7,
    source: function (autocomplete, query, render) {
        var results = [];
        if (query.length === 0) {
            render(results);
            return;
        }

        $$.ajax({
            url: 'https://www.google.ru/complete/search?client=firefox&hl=ru&q=' + query,
            contentType: "application/json",
            crossDomain: true,
            type: "GET",
            dataType: 'json',
            timeout: 2000,
            success: function (data) {
                for (var i = 0; i < 7; i++) {
                    results.push({ id: i, text: data[1][i].toString() });
                }

                render(results);
            }
        });
    }
});

$$("#generate_button").on('touchstart', function (e) {

    if (navigator.connection && navigator.connection.type === Connection.NONE) {
        myApp.alert('Отсутствует подключение к интернету', '');
        return 0;
    }

    var query = $$("#autocomplete-dropdown").val();
    if (!query || query === '') {
        myApp.alert('Введите запрос', '');
        return 0;
    }

    $$("#generate_result").hide();
    $$("#generate_progressbar").show();

    $$.ajax({
        url: 'http://boiling-cove-32035.herokuapp.com/search',
        contentType: "application/json",
        crossDomain: true,
        type: "POST",
        data: JSON.stringify({ searchString: query }),
        dataType: 'json',
        timeout: 120000,
        success: function (data) {
            $$("#generate_result").show();
            $$("#generate_progressbar").hide();
            $$("#result_text").text(data.generatedText);
        },
        error: function () {
            $$("#generate_progressbar").hide();
            myApp.alert('При составлении текста произошла ошибка ', '');
        }
    });

    return 0;
});